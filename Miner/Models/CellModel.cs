﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace Miner.Models
{
    public class CellModel : ICellItem
    {
        private CellState _cellState;
        private Image _stateImage;

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// State of cell
        /// </summary>
        public CellState CellState
        {
            get => _cellState;
            set
            {
                if (_cellState != value)
                {
                    _cellState = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Flag that indicate cell has mine
        /// </summary>
        public bool HasMine { get; set; }

        /// <summary>
        /// Count of mines around
        /// </summary>
        public int MinesCount { get; set; }

        /// <summary>
        /// Text on sell
        /// </summary>
        public string Text
        {
            get => !HasMine && MinesCount > 0 ? MinesCount.ToString() : string.Empty;
        }

        /// <summary>
        /// Image that represents cell state
        /// </summary>
        public Image StateImage
        {
            get => _stateImage;
            set
            {
                if (_stateImage != value)
                {
                    _stateImage = value;
                    OnPropertyChanged();
                }
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
