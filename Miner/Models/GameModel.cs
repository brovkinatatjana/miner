﻿namespace Miner.Models
{
    /// <summary>
    /// Model of game
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Model of field cells 
        /// </summary>
        public CellModel[,] Cells { get; set; }

        /// <summary>
        /// Game state
        /// </summary>
        public GameState GameState { get; set; }
    }
}
