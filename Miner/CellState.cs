﻿namespace Miner
{
    /// <summary>
    /// State of cell
    /// </summary>
    public enum CellState
    {
        /// <summary>
        /// Cell is open
        /// </summary>
        Open,

        /// <summary>
        /// Cell is close
        /// </summary>
        Close,

        /// <summary>
        /// Cell is marked
        /// </summary>
        Marked
    }
}
