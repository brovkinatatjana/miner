﻿namespace Miner
{
    /// <summary>
    /// State of game round
    /// </summary>
    public enum GameState
    {
        Running,
        Win,
        Fail
    }
}