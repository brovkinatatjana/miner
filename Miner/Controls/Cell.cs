﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Miner
{
    public partial class Cell : Control
    {
        /// <summary>
        /// Format for text on cell
        /// </summary>
        private static readonly StringFormat TextFormat = new StringFormat
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
        };

        private ICellItem _item;

        /// <summary>
        /// Color of border
        /// </summary>
        public Color BorderColor { get; set; }

        /// <summary>
        /// Row index of cell
        /// </summary>
        public int RowIndex { get; set; }

        /// <summary>
        /// Column index of cell
        /// </summary>
        public int ColumnIndex { get; set; }

        public Cell()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set cell item
        /// </summary>
        /// <param name="item">Item with cell data</param>
        public void SetItem(ICellItem item)
        {
            if (_item != null)
            {
                _item.PropertyChanged -= OnItemPropertyChanged;
            }

            _item = item;
            _item.PropertyChanged += OnItemPropertyChanged;
        }

        private void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Refresh();
        }

        protected override void OnPaint(PaintEventArgs pea)
        {
            base.OnPaint(pea);
            DrawCell(pea.Graphics);
        }

        protected virtual void DrawCell(Graphics graphics)
        {
            if (_item != null)
            {
                var cellRectangle = new Rectangle(
                    ClientRectangle.X + Padding.Left,
                    ClientRectangle.Y + Padding.Top,
                    ClientRectangle.Width - Padding.Left - Padding.Right,
                    ClientRectangle.Height - Padding.Top - Padding.Bottom);
                switch (_item.CellState)
                {
                    case CellState.Close:
                        ControlPaint.DrawBorder(graphics, cellRectangle, BorderColor, ButtonBorderStyle.Outset);
                        break;
                    case CellState.Open:
                        ControlPaint.DrawBorder(graphics, cellRectangle, BorderColor, ButtonBorderStyle.Inset);
                        DrawImage(graphics, cellRectangle);
                        if (!string.IsNullOrWhiteSpace(_item.Text))
                        {
                            using (var brush = new SolidBrush(ForeColor))
                            {
                                graphics.DrawString(_item.Text, Font, brush, cellRectangle, TextFormat);
                            }
                        }

                        break;
                    case CellState.Marked:
                        ControlPaint.DrawBorder(graphics, cellRectangle, BorderColor, ButtonBorderStyle.Outset);
                        DrawImage(graphics, cellRectangle);
                        break;
                }
            }
        }

        private void DrawImage(Graphics graphics, Rectangle cellRectangle)
        {
            if (_item.StateImage != null)
            {
                var image = _item.StateImage;
                var scale = Math.Min(cellRectangle.Width * 1.0f / image.Width, cellRectangle.Height * 1.0f / image.Height);
                var scaledWidth = image.Width * scale;
                var scaledHeight = image.Height * scale;
                graphics.DrawImage(image, (cellRectangle.Width - scaledWidth) / 2, (cellRectangle.Height - scaledHeight) / 2, scaledWidth, scaledHeight);
            }
        }
    }
}
