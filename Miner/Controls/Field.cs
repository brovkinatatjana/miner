﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Miner
{
    public partial class Field : Panel
    {
        private static readonly object EventCellClick = new object();

        private ICellItem[,] _cellItems;

        public event EventHandler<CellClickEventArgs> CellClick
        {
            add
            {
                Events.AddHandler(EventCellClick, value);
            }
            remove
            {
                Events.RemoveHandler(EventCellClick, value);
            }
        }

        public Field()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create cells with items
        /// </summary>
        /// <param name="cellItems">Items</param>
        public void Bind(ICellItem[,] cellItems)
        {
            DisposeCells(true);
            _cellItems = cellItems;
            var rowsCount = cellItems.GetUpperBound(0) + 1;
            var columnsCount = cellItems.Length / rowsCount;
            var cellWidth = Size.Width / columnsCount;
            var cellHeight = Size.Height / rowsCount;
            for (var rowIndex = 0; rowIndex < rowsCount; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < columnsCount; columnIndex++)
                {

                    var cell = new Cell
                    {
                        RowIndex = rowIndex,
                        ColumnIndex = columnIndex,
                        Width = cellWidth,
                        Height = cellHeight,
                        Location = new Point(cellWidth * columnIndex, cellHeight * rowIndex),
                        Padding = new Padding(1)
                    };
                    cell.SetItem(cellItems[rowIndex, columnIndex]);
                    cell.MouseClick += OnCellClick;
                    Controls.Add(cell);
                }
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                DisposeCells(disposing);
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        protected virtual void OnCellClick(CellClickEventArgs e)
        {
            (Events[EventCellClick] as EventHandler<CellClickEventArgs>)?.Invoke(this, e);
        }

        private void OnCellClick(object sender, MouseEventArgs e)
        {
            var cell = sender as Cell;
            OnCellClick(
                new CellClickEventArgs
                {
                    RowIndex = cell.RowIndex,
                    ColumnIndex = cell.ColumnIndex,
                    IsMarkClick = e.Button == MouseButtons.Right
                });
        }

        /// <summary>
        /// Clean up cells
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        private void DisposeCells(bool disposing)
        {
            if (disposing)
            {
                if (Controls != null && Controls.Count > 0)
                {
                    foreach (var control in Controls)
                    {
                        var cell = control as Control;
                        cell.MouseClick -= OnCellClick;
                        if (!cell.IsDisposed)
                        {
                            cell.Dispose();
                        }
                    }

                    Controls.Clear();
                }
            }
        }
    }
}
