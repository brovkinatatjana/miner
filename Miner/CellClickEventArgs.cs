﻿using System;

namespace Miner
{
    /// <summary>
    /// Cell click data
    /// </summary>
    public class CellClickEventArgs : EventArgs
    {
        /// <summary>
        /// Row index of cell
        /// </summary>
        public int RowIndex { get; set; }

        /// <summary>
        /// Column index of cell
        /// </summary>
        public int ColumnIndex { get; set; }

        /// <summary>
        /// Click to mark cell
        /// </summary>
        public bool IsMarkClick { get; set; }
    }
}
