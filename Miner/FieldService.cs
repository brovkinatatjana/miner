﻿using Miner.Models;
using System;
using System.Drawing;

namespace Miner
{
    public class FieldService
    {
        private readonly Image _markedCellImage;
        private readonly Image _mineImage;
        private GameModel _gameModel;
        private int _rowsCount;
        private int _columnsCount;
        private int[][] _mines;

        /// <summary>
        /// Initialize game servise
        /// </summary>
        /// <param name="markedCellImage">Flag image</param>
        /// <param name="mineImage">Mine image</param>
        public FieldService(Image markedCellImage, Image mineImage)
        {
            _markedCellImage = markedCellImage;
            _mineImage = mineImage;
        }

        /// <summary>
        /// Initialize new game
        /// </summary>
        /// <param name="gameLevel">Game level</param>
        /// <returns>Model of game state</returns>
        public GameModel GetNewGameModel(GameLevel gameLevel)
        {
            int minesCount;
            switch (gameLevel)
            {
                case GameLevel.Simple:
                    _rowsCount = 9;
                    _columnsCount = 9;
                    minesCount = 10;
                    break;
                case GameLevel.Middle:
                    _rowsCount = 16;
                    _columnsCount = 16;
                    minesCount = 40;
                    break;
                case GameLevel.Hard:
                    _rowsCount = 16;
                    _columnsCount = 30;
                    minesCount = 99;
                    break;
                default:
                    throw new NotImplementedException();
            }

            var cells = new CellModel[_rowsCount, _columnsCount];

            for (var rowIndex = 0; rowIndex < _rowsCount; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < _columnsCount; columnIndex++)
                {
                    cells[rowIndex, columnIndex] = new CellModel
                    {
                        CellState = CellState.Close
                    };
                }
            }

            _mines = new int[minesCount][];
            var rnd = new Random();
            while (minesCount > 0)
            {
                var mineRowIndex = rnd.Next(_rowsCount - 1);
                var mineColumnIndex = rnd.Next(_columnsCount - 1);
                var cell = cells[mineRowIndex, mineColumnIndex];
                if (!cell.HasMine)
                {
                    cell.HasMine = true;
                    minesCount--;
                    _mines[minesCount] = new[] { mineRowIndex, mineColumnIndex };
                    var firstRowIndexAround = mineRowIndex > 0 ? mineRowIndex - 1 : mineRowIndex;
                    var maxRowIndexAround = mineRowIndex == _rowsCount - 1 ? mineRowIndex : mineRowIndex + 1;
                    var firstColumnIndexAround = mineColumnIndex > 0 ? mineColumnIndex - 1 : mineColumnIndex;
                    var maxColumnIndexAround = mineColumnIndex == _columnsCount - 1 ? mineColumnIndex : mineColumnIndex + 1;
                    for (var rowIndex = firstRowIndexAround; rowIndex <= maxRowIndexAround; rowIndex++)
                    {
                        for (var columnIndex = firstColumnIndexAround; columnIndex <= maxColumnIndexAround; columnIndex++)
                        {
                            cells[rowIndex, columnIndex].MinesCount++;
                        }
                    }
                }
            }

            _gameModel = new GameModel
            {
                Cells = cells,
                GameState = GameState.Running
            };

            return _gameModel;
        }

        /// <summary>
        /// Сell click processing
        /// </summary>
        /// <param name="rowIndex">Cell row index</param>
        /// <param name="columnIndex">Cell column index</param>
        /// <param name="isMarkedClick">Flag that represent marked click</param>
        /// <returns>Model of game state<</returns>
        public GameModel GetModelAfterClickOnCell(int rowIndex, int columnIndex, bool isMarkedClick)
        {
            if (isMarkedClick)
            {
                var cell = _gameModel.Cells[rowIndex, columnIndex];
                if (cell.CellState == CellState.Marked)
                {
                    cell.CellState = CellState.Close;
                    cell.StateImage = null;
                }
                else if (cell.CellState == CellState.Close)
                {
                    cell.CellState = CellState.Marked;
                    cell.StateImage = _markedCellImage;
                }
            }
            else
            {
                OpenCell(rowIndex, columnIndex);
                if (_gameModel.GameState == GameState.Running)
                {
                    foreach (var cell in _gameModel.Cells)
                    {
                        if (!cell.HasMine && cell.CellState != CellState.Open)
                        {
                            return _gameModel;
                        }
                    }

                    _gameModel.GameState = GameState.Win;
                }
            }

            return _gameModel;
        }

        private void OpenCell(int cellRowIndex, int cellColumnIndex)
        {
            var cells = _gameModel.Cells;
            var cell = cells[cellRowIndex, cellColumnIndex];
            if (cell.CellState == CellState.Marked || cell.CellState == CellState.Open)
            {
                return;
            }

            if (cell.HasMine)
            {
                _gameModel.GameState = GameState.Fail;
                foreach (var mineCellCoordinates in _mines)
                {
                    var mineCell = cells[mineCellCoordinates[0], mineCellCoordinates[1]];
                    mineCell.StateImage = _mineImage;
                    mineCell.CellState = CellState.Open;
                }

                return;
            }

            cell.CellState = CellState.Open;
            if (cell.MinesCount == 0)
            {
                for (var rowIndex = GetFirstIndex(cellRowIndex); rowIndex <= GetMaxIndex(cellRowIndex, _rowsCount); rowIndex++)
                {
                    for (var columnIndex = GetFirstIndex(cellColumnIndex); columnIndex <= GetMaxIndex(cellColumnIndex, _columnsCount); columnIndex++)
                    {
                        if (rowIndex != cellRowIndex || columnIndex != cellColumnIndex)
                        {
                            OpenCell(rowIndex, columnIndex);
                        }
                    }
                }
            }
        }

        private int GetMaxIndex(int currentIndex, int itemsCount)
        {
            return currentIndex == itemsCount - 1 ? currentIndex : currentIndex + 1;
        }

        private int GetFirstIndex(int currentIndex)
        {
            return currentIndex > 0 ? currentIndex - 1 : currentIndex;
        }
    }
}
