﻿using Miner.Resources;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Miner
{
    public partial class MinerForm : Form
    {
        private readonly FieldService _fieldService;

        public MinerForm()
        {
            InitializeComponent();
            _fieldService = new FieldService(Properties.Resources.flag, Properties.Resources.mine);
            field.CellClick += OnFieldCellClick;
            levelsCb.DataSource = Enum.GetValues(typeof(GameLevel)).Cast<Enum>()
            .Select(value => new
            {
                Description = FormText.ResourceManager.GetString(value.ToString()),
                Value = value
            }).ToList();
            levelsCb.DisplayMember = "Description";
            levelsCb.ValueMember = "Value";
            levelsCb.SelectedIndex = 0;
            newGameBtn.Text = FormText.NewGame;
            StartNewGame();
        }

        private void OnFieldCellClick(object sender, CellClickEventArgs e)
        {
            var gameModel = _fieldService.GetModelAfterClickOnCell(e.RowIndex, e.ColumnIndex, e.IsMarkClick);
            if (gameModel.GameState != GameState.Running)
            {
                field.Enabled = false;
                MessageBox.Show(gameModel.GameState == GameState.Fail ? FormText.Lose : FormText.Win);
            }
        }

        private void OnNewGameBtnClick(object sender, EventArgs e)
        {
            StartNewGame();
        }

        private void StartNewGame()
        {
            var level = (GameLevel)levelsCb.SelectedValue;
            var gameModel = _fieldService.GetNewGameModel(level);
            field.Bind(gameModel.Cells);
            field.Enabled = true;
        }
    }
}
