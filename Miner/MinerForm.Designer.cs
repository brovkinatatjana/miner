﻿namespace Miner
{
    partial class MinerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.newGameBtn = new System.Windows.Forms.Button();
            this.levelsCb = new System.Windows.Forms.ComboBox();
            this.field = new Miner.Field();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.field, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.newGameBtn, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.levelsCb, 0, 0);
            this.tableLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.79021F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.20979F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(415, 286);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // newGameBtn
            // 
            this.newGameBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newGameBtn.Location = new System.Drawing.Point(337, 3);
            this.newGameBtn.Name = "newGameBtn";
            this.newGameBtn.Size = new System.Drawing.Size(75, 21);
            this.newGameBtn.TabIndex = 2;
            this.newGameBtn.UseVisualStyleBackColor = true;
            this.newGameBtn.Click += new System.EventHandler(this.OnNewGameBtnClick);
            // 
            // levelsCb
            // 
            this.levelsCb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.levelsCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelsCb.FormattingEnabled = true;
            this.levelsCb.Location = new System.Drawing.Point(3, 3);
            this.levelsCb.Name = "levelsCb";
            this.levelsCb.Size = new System.Drawing.Size(121, 21);
            this.levelsCb.TabIndex = 0;
            // 
            // field
            // 
            this.tableLayoutPanel.SetColumnSpan(this.field, 2);
            this.field.Dock = System.Windows.Forms.DockStyle.Fill;
            this.field.Location = new System.Drawing.Point(3, 30);
            this.field.Name = "field";
            this.field.Size = new System.Drawing.Size(409, 253);
            this.field.TabIndex = 1;
            // 
            // MinerForm
            // 
            this.ClientSize = new System.Drawing.Size(438, 308);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "MinerForm";
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.ComboBox levelsCb;
        private Field field;
        private System.Windows.Forms.Button newGameBtn;
    }
}

