﻿using System.ComponentModel;
using System.Drawing;

namespace Miner
{
    /// <summary>
    /// Model of cell item
    /// </summary>
    public interface ICellItem : INotifyPropertyChanged
    {
        /// <summary>
        /// State of cell
        /// </summary>
        CellState CellState { get; set; }

        /// <summary>
        /// Text on cell
        /// </summary>
        string Text { get; }

        /// <summary>
        /// Image that represents cell state
        /// </summary>
        Image StateImage { get; set; }
    }
}
