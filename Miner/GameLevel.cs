﻿namespace Miner
{
    /// <summary>
    /// Game level
    /// </summary>
    public enum GameLevel
    {
        Simple,
        Middle,
        Hard
    }
}
